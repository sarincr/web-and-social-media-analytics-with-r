
Social media analytics is the process of collecting and analyzing audience data shared on social networks to improve an organization's strategic business decisions. Web analytics is the measurement, collection, analysis, and reporting of web data to understand and optimize web usage.
